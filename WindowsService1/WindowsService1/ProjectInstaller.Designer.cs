﻿
namespace WindowsService1
{
    partial class Installer1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mpc555SettlementserviceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.mpc555SettlementServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // mpc555SettlementServiceProcessInstaller
            // 
            this.mpc555SettlementServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.mpc555SettlementServiceProcessInstaller.Password = null;
            this.mpc555SettlementServiceProcessInstaller.Username = null;
            // 
            // mpc555SettlementserviceInstaller
            // 
            this.mpc555SettlementserviceInstaller.DisplayName = "mpc555SettlementService";
            this.mpc555SettlementserviceInstaller.ServiceName = "mpc555SettlementService";
            this.mpc555SettlementserviceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // Installer1
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.mpc555SettlementserviceInstaller,
            this.mpc555SettlementServiceProcessInstaller});

        }

        #endregion
        private System.ServiceProcess.ServiceInstaller mpc555SettlementserviceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller mpc555SettlementServiceProcessInstaller;
    }
}